export default {
  allSeasons: require('./allSeasons'),
  dlc1: require('./dlc1'),
  dlc2: require('./dlc2'),
  baseGame: require('./baseGame'),
  allItems: require('./allItems'),
  allItemsDeluxe: require('./allItemsDeluxe'),
  strikeGear: require('./strikeGear')
};
